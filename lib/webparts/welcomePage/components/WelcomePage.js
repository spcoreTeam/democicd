var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import * as React from 'react';
//external imports
import { SPComponentLoader } from "@microsoft/sp-loader";
var _image = require('../../Images/ManojNew16x9.png');
var WelcomePage = /** @class */ (function (_super) {
    __extends(WelcomePage, _super);
    function WelcomePage() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    // public componentDidMount(){
    //   $('div[name="Home"]').on('click', function(){
    //     window.location.reload();
    //   })
    // }
    WelcomePage.prototype.render = function () {
        SPComponentLoader.loadCss("https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css");
        return (React.createElement("div", null,
            React.createElement("div", null,
                React.createElement("h5", null, "CEO's Message")),
            React.createElement("hr", null),
            React.createElement("div", null,
                React.createElement("div", { style: { float: "left", width: '21%', padding: '10px' }, className: "mr-3" },
                    React.createElement("img", { src: _image, alt: "", style: { float: "left", borderRadius: '3px', width: '100%' } })),
                React.createElement("div", { style: { textAlign: "justify", padding: '10px' } },
                    React.createElement("p", null, "YASH is on an exponential revenue growth path last several years.  We have become extremely intentional in our customer acquisition, given the focus on building long-term sustainable relationships. We want to help our customers realize business value from their technology investments and enable them to transform themselves in the process."),
                    React.createElement("p", null, "We have been working to transform delivery globally and make it agile, focused on emerging best-practices, adopt global quality practices, drive profitability, and align it to deliver outstanding outcomes for our customers."),
                    React.createElement("p", null, "Project and program management is a central theme in this delivery transformation. We are establishing a Global Project Management Office ( PMO), which will standardize best practices, guide projects to achieve success consistently, ensure strong governance, and develop a pervasive program management practice across the organization that drives delivery excellence."),
                    React.createElement("p", null, "While  Derek Dyer is leading the Global PMO, Shivpal Singh and his team will run the India based central PMO.  They will interact and collaborate with Service lines, Sales, and Delivery teams in managing/helping manage critical projects."),
                    React.createElement("p", null,
                        "Key focus areas of the Global PMO include",
                        React.createElement("br", null),
                        React.createElement("ul", null,
                            React.createElement("li", null, "Consolidating PM Best practices within ( and from outside of) the organization and strengthen PM PMI methodologies within YASH."),
                            React.createElement("li", null, "Outlining a consistent and well-structured methodology that governs all projects, safeguarding against project failures."),
                            React.createElement("li", null, "Maintaining consistency between Projects  Managers by providing them access to templates, tools, repositories and other knowledge assets."),
                            React.createElement("li", null, "Train-enable-evaluate-certify project and program managers in line with YASH requirements."),
                            React.createElement("li", null, "Act as back up, join or lead large projects as necessary."),
                            React.createElement("li", null, "Champion PM within YASH."))),
                    React.createElement("p", null, "Currently, many projects in YASH Americas and Europe of the SAP Serviceline are being governed-managed by the Global PMO. Starting August 2020, projects in these regions of the ILM and Digital service lines will come under the ambit of the PMO. Over time the idea is to bring the governance of all projects across regions under the stewardship of the Global PMO."),
                    React.createElement("p", null, "In the long term, we will also establish a PM academy as part of the Global PMO, which will train-enable-evaluate-certify project managers and keep them abreast of the latest and greatest in the world of Program management."),
                    React.createElement("br", null),
                    React.createElement("p", null, "This is a strategic and business-critical initiative. I look forward to the leadership and other stakeholders working closely with the Global PMO in ensuring that every project that YASH delivers is exemplary and drives value for our clients.")))));
    };
    return WelcomePage;
}(React.Component));
export default WelcomePage;
//# sourceMappingURL=WelcomePage.js.map