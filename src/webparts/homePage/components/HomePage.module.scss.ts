/* tslint:disable */
require("./HomePage.module.css");
const styles = {
  homePage: 'homePage_3e067cb9',
  container: 'container_3e067cb9',
  row: 'row_3e067cb9',
  column: 'column_3e067cb9',
  'ms-Grid': 'ms-Grid_3e067cb9',
  title: 'title_3e067cb9',
  subTitle: 'subTitle_3e067cb9',
  description: 'description_3e067cb9',
  button: 'button_3e067cb9',
  label: 'label_3e067cb9'
};

export default styles;
/* tslint:enable */